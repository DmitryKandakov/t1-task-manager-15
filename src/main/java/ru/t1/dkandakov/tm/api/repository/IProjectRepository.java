package ru.t1.dkandakov.tm.api.repository;

import ru.t1.dkandakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    Project create(String name, String description);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    boolean existsById(String projectId);

}